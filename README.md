# Ansbile integration in Jenkins (project Description)
Created and configured a dedicated server for Ansible Control Node
Wrote Ansible Playbook, which configures 2 EC2 Instances
Added ssh key file credentials in Jenkins for Ansible Control Node server and Ansible Managed Node
servers
Configured Jenkins to execute the Ansible Playbook on remote Ansible Control Node server as part of the CI/CD pipeline
## So the Jenkinsfile configuration will do the following:
Connect to the remote Ansible Control Node server
Copy Ansible playbook and configuration files to the remote Ansible Control Node server
Copy the ssh keys for the Ansible Managed Node servers to the Ansible Control Node server
Install Ansible, Python3 and Boto3 on the Ansible Control Node server
With everything installed and copied to the remote Ansible Control Node server, execute the
playbook remotely on that Control Node that will configure the 2 EC2 Managed Nodes